<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="http://evamalia.me"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Eva Malia - meditation</title>
 <link>http://evamalia.me/tags/meditation</link>
 <description></description>
 <language>en</language>
<item>
 <title>Heart Zen Meditation Meetup in Providence </title>
 <link>http://evamalia.me/article/heart-zen-meditation-meetup-providence</link>
 <description>


&lt;div class=&quot;yoxview clearfix&quot;&gt;

    &lt;div class=&quot;yoxview-image-preview&quot;&gt;
        &lt;div class=&quot;field-type-image&quot;&gt;
        &lt;a href=&quot;http://evamalia.me/sites/default/files/field/image/articles/IMG_1456.JPG&quot;&gt;
        &lt;img src=&quot;http://evamalia.me/sites/default/files/styles/large/public/field/image/articles/IMG_1456.JPG?itok=u2jg7U2k&quot; alt=&quot;&quot; title=&quot;&quot;/&gt;
        &lt;/a&gt;
        
                
        &lt;/div&gt;
    &lt;/div&gt;

	  
    
    
&lt;/div&gt;

&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Lately, I have met a lot of people inside and outside my counseling practice who have shown a strong interest in incorporating meditation into their lives. They hear the news, they listen to meditation tapes, they read books, and see it all over the internet: meditation improves your focus, your memory and your well-being. Meditation promotes healthy social relationships, increases our sense of gratefulness, and guide us towards a more meaningful life. At the same time many of these people say that they don&#039;t know how to begin a meditation practice or where to go to begin one, while others feel intimidated by attending the long Buddhist rituals and chants that often accompany meditation learning and practice.&lt;/p&gt;

&lt;p&gt;I am a proclaimed Buddhist, and and I use meditation as a spiritual way to increase my level of consciousness. However meditation does not need to be a religious practice to everyone, and not everyone sees it as a spiritual way to reach enlightenment. Sitting there watching your thoughts pass by and letting them go can be just a good practice of common sense, a healthy training for the brain, and a true way of being here now. Being present liberate us from worry and anxiety about the future, and from the guilt and shame of the past. So whichever your belief or faith, you can use meditation to your advantage to be more aware, for self-knowledge, and to increase focus on the things that are really important to you.&lt;/p&gt;

&lt;p&gt;For anyone that would like to begin or continue a meditation practice, I have created a meetup group in Providence  directed to promote a community committed to self-growth, wisdom and compassion. The Heart Zen Meditation Group will be meeting at different times indoors and outdoors, and hopes to reach out to anyone who wants to access the healing practice of a tradition that has lived among us for thousands of years, and that is beneficial to everyone of us.&lt;/p&gt;

&lt;p&gt;For more information head on over to our &lt;a href=&quot;http://meetu.ps/1CdGMq&quot;&gt;meetup group page&lt;/a&gt;.&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-field-tags field-type-taxonomy-term-reference field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/meditation&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;meditation&lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;field-item odd&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/group-meditation&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;group meditation&lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/zen&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Zen&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Thu, 06 Jun 2013 21:35:17 +0000</pubDate>
 <dc:creator>evamalia</dc:creator>
 <guid isPermaLink="false">65 at http://evamalia.me</guid>
</item>
<item>
 <title> Using Meditation to Relieve Stress and Help with Anxiety (Part 2 of 2)</title>
 <link>http://evamalia.me/article/using-meditation-relieve-stress-and-help-anxiety-part-2-2</link>
 <description>


&lt;div class=&quot;yoxview clearfix&quot;&gt;

    &lt;div class=&quot;yoxview-image-preview&quot;&gt;
        &lt;div class=&quot;field-type-image&quot;&gt;
        &lt;a href=&quot;http://evamalia.me/sites/default/files/field/image/articles/_MG_0202.jpg&quot;&gt;
        &lt;img src=&quot;http://evamalia.me/sites/default/files/styles/large/public/field/image/articles/_MG_0202.jpg?itok=mxCMJen0&quot; alt=&quot;&quot; title=&quot;&quot;/&gt;
        &lt;/a&gt;
        
                
        &lt;/div&gt;
    &lt;/div&gt;

	  
    
    
&lt;/div&gt;

&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;When we sit in meditation, we begin to observe the patterns of thought and motifs which run in our minds automatically, and which account for much of the way we feel and react. As we breathe in and out during meditation, we attempt to observe thoughts entering our mind, and leaving our mind, letting go of them gently. The purpose of meditation is to stop clinging to the thoughts that appear on auto-pilot and are greatly responsible for our stress and anxiety. When thoughts come up, we refrain from judging them as good or bad, or as something to get rid of or ignore. Instead, we observe them, let them go and return to noticing the breath. Letting go of our thoughts regularly develops awareness and focus: the two states of mind which disappear when stress or anxiety overwhelm us.&lt;/p&gt;
&lt;p&gt;The purpose is also to reduce aversion or the rejection we feel towards the “noises” of life, which during our meditation period may appear as the sound of the fan, the dog’s bark outside etc. When we act out of awareness, these experiences put us in a bad mood, and we try to avoid them, escape them, or fight them. However, discomfort is reduced for the regular meditator who practices staying with a tolerable degree of everyday challenges and pain and who learns to be present with the inevitable ups and downs of life. Life is more fulfilling when we embrace our joy and our pain equally.&lt;/p&gt;
&lt;p&gt;But what do we do during meditation? Some people count their breath one to ten again and again in order to develop concentration and focus of the mind. It’s a good practice. Counting may be interrupted by distracting thoughts, and when this interruption happens, we strive to let the thought go and start counting all over again. This process continues for the time that the meditation period lasts.&lt;/p&gt;
&lt;p&gt;By practicing meditation every day, we learn to open our senses to the immediate experiences surrounding us, and by doing so we let go of the thoughts that make us worried, stressed, anxious. When we are really present we notice our body and our thoughts, but also the trees that surround us, or the sound of the children playing outside, etc.&lt;/p&gt;
&lt;p&gt;This practice begins by sitting on a cushion or a chair and breathing quietly. Something so simple can transform our everyday life. When we are at work we focus on the task at hand because nothing else exists at that moment. When playing with children, there is nothing else but enjoying that moment. Even cleaning the dishes and doing the laundry can be rewarding when we choose to immerse ourselves in the experience and let go of thoughts of complaint and dissatisfaction.&lt;/p&gt;
&lt;p&gt;Having a meditation practice will help you feel more in control of your mind. You will know yourself better, and begin to change impulsive reactions towards appropriate responses that are more fulfilling. You will understand how and why other people react to you with anger, sadness or joy because you will have experienced intimately and accepted these feelings fully during your practice while letting go of judgment. So if your boss or your partner snaps at you unexpectedly, you may feel anger, sadness, or embarrassment. However your practice of being aware and embracing of your feelings without clinging to them will help you take a pause instead of reacting to your boss, find an adequate response to the situation, and recover faster from the mishappening.&lt;/p&gt;
&lt;p&gt;Our repertoire of choices increases when we are aware of and present in the moment. We are able to take more conscious decisions, and we can understand the impact of our actions and decisions on our life. It will be more likely that we will stop blaming others for our problems and begin seeing how we affect our outcomes. We will recognize when we should act to change a situation that troubles us, and when we can let go of trying to control the forces that escape our human power.&lt;/p&gt;
&lt;p&gt;Meditation practice is for everyone, even for those who feel very anxious about sitting quietly in silence. A couple of minutes a day of creating a pause and breathing with awareness is better than nothing. Meditation practice paves the way to a higher consciousness of self-love and universal compassion.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-field-tags field-type-taxonomy-term-reference field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/meditation&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;meditation&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Sat, 01 Sep 2012 17:15:11 +0000</pubDate>
 <dc:creator>evamalia</dc:creator>
 <guid isPermaLink="false">50 at http://evamalia.me</guid>
</item>
<item>
 <title>Using Meditation to Relieve Stress and Help with Anxiety (Part 1 of 2) </title>
 <link>http://evamalia.me/article/using-meditation-relieve-stress-and-help-anxiety-part-1-2</link>
 <description>


&lt;div class=&quot;yoxview clearfix&quot;&gt;

    &lt;div class=&quot;yoxview-image-preview&quot;&gt;
        &lt;div class=&quot;field-type-image&quot;&gt;
        &lt;a href=&quot;http://evamalia.me/sites/default/files/field/image/articles/_MG_4728%20-%20Version%202.jpg&quot;&gt;
        &lt;img src=&quot;http://evamalia.me/sites/default/files/styles/large/public/field/image/articles/_MG_4728%20-%20Version%202.jpg?itok=ajB_glaI&quot; alt=&quot;&quot; title=&quot;&quot;/&gt;
        &lt;/a&gt;
        
                
        &lt;/div&gt;
    &lt;/div&gt;

	  
    
    
&lt;/div&gt;

&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;More and more of us are turning to meditation to relieve stress caused by our high-paced tech-enabled lifestyles. It is becoming a common practice to use meditation to deal with fears of the unknown, the uncontrollable and the uncertain. Such interest in meditation has attracted the curiosity of many researchers who have found evidence to confirm the beneficial effects of meditation on mental health.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;What is Meditation?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;Meditation is the practice of being aware of what is going on within and around us. It is akin to listening with the whole body. In a position of sitting, walking, or laying quietly, we begin by focusing on breath. As we breathe, we open our senses to hearing, smelling, seeing and feeling what is being experienced right now. Being present means noticing our breath, but also noticing the plane passing in the sky, the dog barking, the birds chirping, and the leaves of trees blowing in the wind.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;How does meditation work?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;When practiced over time meditation builds strength from the inside out and is a tool which is always available. Anxiety drugs offer a chemical quick fix to the person who feels anxious though it can be a needed first step in some cases. Meditation on the other hand builds intrinsic coping skills, entailing a richer, more effective process that creates new neural pathways and connections in the brain. These changes to the physical brain are then more permanent and affect our ability to control and regulate our stress, mood, and physical health. (To read more about studies on how meditation changes brain structure &lt;a href=&quot;http://www.huffingtonpost.com/2012/06/15/mindfulness-meditation-brain-integrative-body-mind-training_n_1594803.html&quot;&gt;click here&lt;/a&gt;) When we are meditating we notice our thoughts appear as thoughts, and our tendency to automatically entertain them and the feelings they carry. This is what the Buddhists call the “monkey mind”. The monkey mind is a chain of relatively disconnected thoughts which jump from one to another without awareness. For example, a meditator would become aware of the following chain of thoughts:&lt;/p&gt;
&lt;blockquote&gt;&lt;p&gt;&quot;What are we going to have for dinner today?... I am terrible at planning meals... Remember those meals mom used to make after school?...Ah, school, I never liked school!... I enjoyed spending time with my friend Laura though.... What’s Laura up to these days?... I wish I had a best friend like her nowadays!. But what I really want is to be in a good relationship. What am I looking for in a partner anyway? Someone to go to Italy with. Ah, Italy...”&lt;/p&gt;&lt;/blockquote&gt;
&lt;p&gt;The monkey mind is our default mind when we have not trained ourselves to be aware of what and how we think. When we are on auto-pilot we tend to feel that our thoughts are running our life. We cannot see that we actually have a choice over what we think or feel, or even that we can choose to believe our thoughts or not believe our thoughts altogether. Habitual thoughts can lead to high levels of stress and anxiety and overwhelm us.&lt;/p&gt;
&lt;p&gt;It is possible to learn to choose more often than not the types of thoughts we give permission to linger in our minds. One way to do this is through the practice of awareness and letting go of our thoughts during meditation. In my &lt;a href=&quot;http://evamalia.me/article/using-meditation-relieve-stress-and-help-anxiety-part-2-2&quot;&gt;next post&lt;/a&gt;, I’ll share how we can take our meditation practice into our everyday lives to bring a sense of peace and joy to our most dreaded tasks, and also how meditation changes the way we respond to difficult situations.&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Eva Malia is a licensed mental health counselor and works with clients on anxiety and meditation in her private practice. To schedule a time to work with Ms. Malia, fill out this &lt;a href=&quot;http://evamalia.me/inquiry-form&quot;&gt;inquiry form&lt;/a&gt;.&lt;/em&gt;&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-field-tags field-type-taxonomy-term-reference field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/meditation&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;meditation&lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;field-item odd&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/anxiety-relief&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;anxiety relief&lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;dc:subject&quot;&gt;&lt;a href=&quot;/tags/stress-relief&quot; typeof=&quot;skos:Concept&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;stress relief&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Sun, 01 Jul 2012 17:39:34 +0000</pubDate>
 <dc:creator>evamalia</dc:creator>
 <guid isPermaLink="false">48 at http://evamalia.me</guid>
</item>
</channel>
</rss>
